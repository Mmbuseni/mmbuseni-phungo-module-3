import 'package:flutter/material.dart';

class ProfileEdit extends StatelessWidget {
  const ProfileEdit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: const Text('Edit Profile'),
            centerTitle: true,
            backgroundColor: const Color.fromARGB(255, 190, 26, 215)),
        body: Container(
            padding: const EdgeInsets.all(40),
            alignment: Alignment.center,
            child: Column(children: const <Widget>[
              Padding(padding: EdgeInsets.all(10)),
              TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Username',
                      hintText: 'Enter your new username')),
              Padding(padding: EdgeInsets.all(10)),
              TextField(
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Password',
                      hintText: 'Update password'))
            ])));
  }
}
