import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('LOG IN'),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 190, 26, 215)),
      body: Container(
        padding: const EdgeInsets.all(40),
        alignment: Alignment.center,
        child: Column(
          children: const <Widget>[
            Padding(padding: EdgeInsets.all(10)),
            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Username',
                  hintText: 'Enter your email address'),
            ),
            Padding(padding: EdgeInsets.all(10)),
            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Password',
                  hintText: 'Enter your password'),
            ),
            Padding(padding: EdgeInsets.all(40)),
            ElevatedButton(onPressed: null, child: Text('SIGN UP')),
            Text('Forgot Password? Reset Password')
          ],
        ),
      ),
    );
  }
}
