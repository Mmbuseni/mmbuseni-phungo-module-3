import 'package:flutter/material.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text('SIGN UP'),
          centerTitle: true,
          backgroundColor: const Color.fromARGB(255, 190, 26, 215)),
      body: Container(
          alignment: Alignment.center, padding: const EdgeInsets.all(40)),
    );
  }
}
